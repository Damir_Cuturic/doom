;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "John Doe"
      user-mail-address "john@doe.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-nord)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
;(setq org-directory "~/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.
;;(setq doom-font (font-spec :family "Fira Code" :size 14))
(setq doom-font (font-spec :family "Fantasque Sans Mono" :size 18))

;; Tweaks from https://zzamboni.org/post/my-doom-emacs-configuration-with-commentary/
;; Stop Emacs from comfirming the :q action I want it to quit when I say so
(setq confirm-kill-emacs nil)

;; Enable autosave
(setq auto-save-default t
      make-backup-files t)

;; Maximize swindow at startup
(add-to-list 'initial-frame-alist '(fullscreen . maximized))

;;   ___  ____   ____       __  __  ___  ____  _____
;;  / _ \|  _ \ / ___|     |  \/  |/ _ \|  _ \| ____|
;; | | | | |_) | |  _ _____| |\/| | | | | | | |  _|
;; | |_| |  _ <| |_| |_____| |  | | |_| | |_| | |___
;;  \___/|_| \_\\____|     |_|  |_|\___/|____/|_____|

;; FIXME
;; Set default directory for org files
;; do and if Linux then that if Windows then that
(setq org-directory "C:\webdav" )

;; FIXME
;; Org-mode stuff
;; adding to the C-c C-, a template
;; https://emacs.stackexchange.com/questions/40571/how-to-set-a-short-cut-for-begin-src-end-src
;; BUG DOOM startup crashes with this line
;; (add-to-list 'org-structure-template-alist '("b" . "src bash"))

;; NOTE this enables the <s TAB combo to quickly add the blocks
(require 'org-tempo)

;;       _   _ _        _____ ___  ____   ___
;;      | | | | |      |_   _/ _ \|  _ \ / _ \
;;      | |_| | |   _____| || | | | | | | | | |
;;      |  _  | |__|_____| || |_| | |_| | |_| |
;;      |_| |_|_____|    |_| \___/|____/ \___/
;;
;;      Configuration that has impact on hl-todo-mode
;;      REVIEW https://github.com/tarsius/hl-todo
;;      REVIEW https://www.emacswiki.org/emacs/FixmeMode
;;      TODO This section needs some research, bacause RESEARCH keyword is staying white but us getting bolded, so something is working
;;      TODO Do I need to add all of the existing keywords, or is there a way to prevent overriding
;;
;;
;; (after! hl-todo
;;    (setq hl-todo-keyword-faces
;;          `(;; For things that need to be done, just not today.
;;       ("TODO" warning bold)
;;       ;; For problems that will become bigger problems later if not
;;       ;; fixed ASAP.
;;       ("FIXME" error bold)
;;       ;; For tidbits that are unconventional and not intended uses of the
;;       ;; constituent parts, and may break in a future update.
;;       ("HACK" font-lock-constant-face bold)
;;       ;; For things that were done hastily and/or hasn't been thoroughly
;;       ;; tested. It may not even be necessary!
;;       ("REVIEW" font-lock-keyword-face bold)
;;       ;; For especially important gotchas with a given implementation,
;;       ;; directed at another user other than the author.
;;       ("NOTE" success bold)
;;       ;; For things that just gotta go and will soon be gone.
;;       ("DEPRECATED" font-lock-doc-face bold)
;;       ;; For a known bug that needs a workaround
;;       ("BUG" error bold)
;;       ;; More research is needed for this topic
;;       ("RESEARCH" "#3A3769" bold))))
